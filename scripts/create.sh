#!/bin/bash

# Update repo to the latest version
git pull --ff

# Update feeds
./scripts/feeds update -a

# Install packages
./scripts/feeds install -a

# Run router configs builder
make menuconfig

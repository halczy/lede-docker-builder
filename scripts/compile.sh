#!/bin/bash

# Update repo to the latest version
git pull --ff

# Update feeds
./scripts/feeds update -a && ./scripts/feeds install -a

# Read configs
make defconfig

# Download packages
make download -j$(nproc)

# Comple
make V=s -j$(nproc)

#!/bin/bash

echo "Looking for config file..."
CONFIG_FILE=$(ls router-configs | head -n 1)

echo "Moving config file..."
cp ./router-configs/$CONFIG_FILE .config

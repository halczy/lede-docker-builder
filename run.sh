#!/bin/bash

IMAGE="lede-builder"

echo "Building docker image..."
docker image build . -t $IMAGE

echo "Running builder in Docker..."
docker run -v $PWD/router-configs:/home/builder/lede/router-configs -v $PWD/firmwares:/home/builder/lede/firmwares -it $IMAGE

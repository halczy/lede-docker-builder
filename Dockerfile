FROM debian:bullseye-slim

# Set timezone
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Set Debian mirror
# RUN sed -i 's@//.*deb.debian.org@//mirrors.ustc.edu.cn@g' /etc/apt/sources.list

# Install essential packages
RUN apt update -y && \
  apt full-upgrade -y && \
  DEBIAN_FRONTEND=noninteractive apt install -y ack antlr3 aria2 asciidoc autoconf automake autopoint binutils bison build-essential \
  bzip2 ccache cmake cpio curl device-tree-compiler fastjar flex gawk gettext gcc-multilib g++-multilib \
  git gperf haveged help2man intltool libc6-dev-i386 libelf-dev libglib2.0-dev libgmp3-dev libltdl-dev \
  libmpc-dev libmpfr-dev libncurses5-dev libncursesw5-dev libreadline-dev libssl-dev libtool lrzsz \
  mkisofs msmtp nano ninja-build p7zip p7zip-full patch pkgconf python2.7 python3 python3-pip libpython3-dev qemu-utils \
  rsync scons squashfs-tools subversion swig texinfo uglifyjs upx-ucl unzip vim wget xmlto xxd zlib1g-dev python3-distutils sudo

# Passwordless sudo and non-root user
RUN useradd -m builder && echo 'builder ALL=NOPASSWD: ALL' > /etc/sudoers.d/builder
USER builder
WORKDIR /home/builder

# Clone Lede Repo
RUN git clone https://github.com/coolsnowwolf/lede && cd lede
WORKDIR /home/builder/lede

# Copy default feeds
COPY feeds.conf.default /home/builder/lede
# Update and install packages
RUN ./scripts/feeds update -a && ./scripts/feeds install -a

# Copy scripts
COPY scripts/*.sh /home/builder/lede

# Add permissions
RUN sudo chmod +x *.sh

# CUSTOM Settings
RUN sed -i 's/192.168.1.1/192.168.10.1/g' package/base-files/files/bin/config_generate

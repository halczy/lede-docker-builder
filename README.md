# LEDE Docker Builder

This is a project that runs a builder in Docker container for the OpenWRT fork [LEDE](https://github.com/coolsnowwolf/lede)

## Create new router firmware configs and compile firmware

### Create and run docker container

Create and build a docker image in order to run the the firmware creation wizard by running the following commands.

```bash
sudo chmod +x run.sh
./run.sh
```

### Create router firmware config file

Inside the docker container, run the following command to start up the creation wizard. Specify the kind of router and packages you would like to install. ** Save the file as the default .config ** to avoid complication down the road.

```bash
./create.sh
```

### Compile firmware

Once your `.config` router configuration file is ready. Run the following command.

```bash
./compile.sh
```

### Transfer the firmware

Once your firmware is successfully compiled. Transfer the firmware to the `firmwares` folder.

```bash
./transfer.sh
```

## Provide existing config and compile new firmware

If you already have an existing router firmware config and would like a compile a new firmware. Just run
the following command.

```
sudo chmod +x compile_firmware.sh
./compile_firmware.sh
```

After a successful run, you should be able to find the compiled firmware under the firmwares folder.

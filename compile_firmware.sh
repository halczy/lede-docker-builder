#!/bin/bash

IMAGE="lede-builder"

echo "Building docker image..."
docker image build . -t $IMAGE

echo "Transfering config files..."
docker run -v $PWD/router-configs:/home/builder/lede/router-configs -v $PWD/firmwares:/home/builder/lede/firmwares $IMAGE sh transfer_config.sh

echo "Compiling..."
docker run -v $PWD/router-configs:/home/builder/lede/router-configs -v $PWD/firmwares:/home/builder/lede/firmwares $IMAGE sh compile.sh

echo "Transfering firmware..."
docker run -v $PWD/router-configs:/home/builder/lede/router-configs -v $PWD/firmwares:/home/builder/lede/firmwares $IMAGE sh transfer_firmware.sh

